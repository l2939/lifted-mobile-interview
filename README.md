# Lifted Mobile Interview

Lifted's flutter apps use a architecture which is based on [_MVVM_](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel)
i.e. _Model-View-ViewModel_. This architecture helps ensure a separation of concerns between UI elements and the
application's business logic.

![MVVM doc](https://miro.medium.com/max/1212/1*BpxMFh7DdX0_hqX6ABkDgw.png)

Each screen has a corresponding view model which manages all of it's state as well as interactions with services such as the
API or local storage etc. For the purposes of this test you do not need to consider the model.

This project uses the [provider](https://pub.dev/packages/provider) package under the hood to manage the state which is kept in the view models. Each view model
is a `ChangeNotifierProvider` so be sure to use provider's patterns when changing values in a view model. You do not need to create any new providers yourself or change
any of the inner workings of app architecture for this test.

#### Take home

Add a new screen to the app which allows a user to add in their:

- First name
- Last name
- Email
- Phone number
- 2 checkboxes asking if they
  - accept our terms and conditions, this doesn't need to link anywhere, it should just be a check box that collects this
    agreement.
  - collects the user's consent to send marketing emails (if they agree)

This form should ensure all values are populated and non-empty and show an error message if a value is missing.
A user should not be able to submit an incomplete form. The values of this form should be sent to the backend using the
`APIService`.

#### Pair programming

Two new fields need to be added to hooked up to collect a user's email and password so they can be
logged into the app. Wire up the provided inputs so that they collect a user's information save the values
in state using the app's architecture. This information should then be sent to the API using the `APIService`

#### Stretch goal

Consider a situation where the app is offline yet a user would like to sign up and we would rather allow this request to happen offline and
let the user sign up optimistically.

Find a way to persist the request offline and optimistically update the UI and let the user sign in. When the connection status changes and the
user is back online re-execute all previous offline requests.

## Considerations

Where possible the solution to each task should be implemented with respect to the projects structure and architecture.
Screens should be placed with existing screens and where possible and relevant new files should be placed with this in mind.
Please try to ensure your solution is scalable and maintainable.
