import 'package:flutter/material.dart';

import '../widgets/page.dart';

/// This page should collect a users email and password and send this to the
/// backend via the [APIService]. The screen itself should not contain any
/// business logic. It should use the app's architecture to handle state
/// management.
class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TIP: consider adding a ViewModelProvider to this page. see the [HomeScreen] for an example.
    return LiftedPage(
      title: 'Login',
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            // TODO: add email and password to state and then pass them on the
            // the backend.
            TextFormField(
              decoration: const InputDecoration(labelText: 'Email'),
            ),
            TextFormField(
              decoration: const InputDecoration(labelText: 'Password'),
            ),
          ],
        ),
      ),
    );
  }
}
