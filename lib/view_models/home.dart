import 'package:lifted_mobile_interview/services/navigation.dart';

import 'base.dart';

class HomeViewModel extends BaseViewModel {
  final NavigationService _navigationService = NavigationService.instance;
  final title = 'Home';

  void goToSignUp() {
    _navigationService.pushNamed('/signup');
  }

  void login() {
    _navigationService.pushNamed('/login');
  }
}
