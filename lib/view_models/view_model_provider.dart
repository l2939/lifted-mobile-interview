import 'package:flutter/material.dart';
import 'package:lifted_mobile_interview/view_models/base.dart';
import 'package:provider/provider.dart';

class ViewModelProvider<T extends BaseViewModel> extends StatefulWidget {
  final T model;
  final Widget Function(BuildContext context, T model) builder;

  const ViewModelProvider({
    Key? key,
    required this.model,
    required this.builder,
  }) : super(key: key);

  @override
  State<ViewModelProvider<T>> createState() => _ViewModelProviderState<T>();
}

class _ViewModelProviderState<T extends BaseViewModel> extends State<ViewModelProvider<T>> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      create: (context2) => widget.model,
      child: Consumer<T>(
        builder: (BuildContext context, T model, Widget? child) {
          return widget.builder(context, model);
        },
      ),
    );
  }
}
