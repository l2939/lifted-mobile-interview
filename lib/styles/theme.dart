import 'dart:math';

import 'package:flutter/material.dart';

class LiftedTheme {
  static const primaryColor = Color(0xFF5D2FE9);
  static const primaryColorLight = Color(0xFF323F7D);
  static const errorColor = Color(0xFFF85971);
  static const primaryColorDark = Color(0xFF202B63);

  static ThemeData get theme {
    return ThemeData(
      primarySwatch: generateMaterialColor(primaryColor),
      errorColor: errorColor,
      primaryColor: primaryColor,
      primaryColorDark: primaryColorDark,
    );
  }

  // NOTE: this is not relevant to the task and is simply here to make the
  // project a little nicer to look at/match our brand colours.
  static MaterialColor generateMaterialColor(Color color) {
    return MaterialColor(color.value, {
      50: tintColor(color, 0.9),
      100: tintColor(color, 0.8),
      200: tintColor(color, 0.6),
      300: tintColor(color, 0.4),
      400: tintColor(color, 0.2),
      500: color,
      600: shadeColor(color, 0.1),
      700: shadeColor(color, 0.2),
      800: shadeColor(color, 0.3),
      900: shadeColor(color, 0.4),
    });
  }

  static int tintValue(int value, double factor) {
    return max(0, min((value + ((255 - value) * factor)).round(), 255));
  }

  static Color tintColor(Color color, double factor) {
    return Color.fromRGBO(
      tintValue(color.red, factor),
      tintValue(color.green, factor),
      tintValue(color.blue, factor),
      1,
    );
  }

  static int shadeValue(int value, double factor) {
    return max(0, min(value - (value * factor).round(), 255));
  }

  static Color shadeColor(Color color, double factor) {
    return Color.fromRGBO(
      shadeValue(color.red, factor),
      shadeValue(color.green, factor),
      shadeValue(color.blue, factor),
      1,
    );
  }
}
