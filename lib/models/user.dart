
class User {
  final String firstName;
  final String lastName;
  final String email;
  final String phoneNumber;
  final bool marketingConsent;
  final bool termsAndConditionsConsent;

  User({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.phoneNumber,
    required this.marketingConsent,
    required this.termsAndConditionsConsent,
  });
}
