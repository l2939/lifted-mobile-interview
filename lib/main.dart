import 'package:flutter/material.dart';
import 'package:lifted_mobile_interview/styles/theme.dart';

import 'screens/home.dart';
import 'screens/login.dart';
import 'services/navigation.dart';
import 'widgets/page.dart';

void main() {
  runApp(const LiftedApp());
}

/// The objective of this test is to either add a new screen or wire some inputs
/// to collect a user's data and sends it to the API using the API service.
/// This screen should mirror the architecture that the app uses.
/// Each screen should only contain presentational components. All logic is
/// deferred to the specific view model for that screen. The view model
/// maintains state and interacts with services such as the Navigation or API
/// service.
class LiftedApp extends StatelessWidget {
  const LiftedApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        // TODO(take home test): Add a functioning signup screen.
        '/signup': (context) =>
            const LiftedPage(title: 'Signup', child: Placeholder()),
        // TODO(pair programming): Wire up the existing login screen.
        '/login': (context) => const LoginScreen(),
      },
      navigatorKey: NavigationService.navigator,
      title: 'Lifted',
      theme: LiftedTheme.theme,
      home: const HomeScreen(title: 'Lifted'),
    );
  }
}
