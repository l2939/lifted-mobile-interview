import 'package:flutter/material.dart';

class LiftedPage extends StatelessWidget {
  final Widget child;
  final String? title;
  const LiftedPage({Key? key, required this.child, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title ?? 'Lifted'),
      ),
      body: child,
    );
  }
}
