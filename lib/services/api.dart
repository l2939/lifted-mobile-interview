import 'dart:math';

import '../models/user.dart';

class APIService {
  Future<LoginResponse> loginUser(String email, String password) {
    return Future.delayed(const Duration(seconds: 1), () {
      return __randomiseSuccess(
        'Failed to login user',
        LoginResponse(
          success: true,
          user: User(
            firstName: 'John',
            lastName: 'Doe',
            email: 'john@yahoo234.com',
            marketingConsent: false,
            termsAndConditionsConsent: true,
            phoneNumber: '+1-234-567-8910',
          ),
        ),
      );
    });
  }

  Future<RegistrationResponse> registerUser({
    required String firstName,
    required String lastName,
    required String email,
    required String phoneNumber,
    required bool marketingConsent,
    required bool termsAndConditionsConsent,
  }) {
    return Future.delayed(const Duration(seconds: 1), () {
      return __randomiseSuccess(
        'Failed to sign up $firstName',
        RegistrationResponse(
          success: true,
          user: User(
            firstName: firstName,
            lastName: lastName,
            email: email,
            phoneNumber: phoneNumber,
            marketingConsent: marketingConsent,
            termsAndConditionsConsent: termsAndConditionsConsent,
          ),
        ),
      );
    });
  }

  // NOTE: do NOT touch or use this method
  T __randomiseSuccess<T>(String errorMessage, T value) {
    final rnd = Random();
    final guess = rnd.nextBool();
    if (guess) throw Exception(errorMessage);
    return value;
  }
}

class LoginResponse {
  final bool success;
  final User user;

  LoginResponse({required this.success, required this.user});
}

class RegistrationResponse {
  final bool success;
  final User user;

  RegistrationResponse({required this.success, required this.user});
}
