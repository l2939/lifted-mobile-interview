import 'package:flutter/material.dart';

class NavigationService {
  static final instance = NavigationService.__private();
  NavigationService.__private();

  static final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();

  Future<dynamic>? pushNamed(String routeName, {Object? arguments}) {
    return navigator.currentState?.pushNamed(
      routeName,
      arguments: arguments,
    );
  }

  Future<dynamic>? pushReplacementNamed(String routeName, {Object? arguments}) {
    return navigator.currentState?.pushReplacementNamed(
      routeName,
      arguments: arguments,
    );
  }

  Future<dynamic>? pushNamedAndClearHistory(
    String routeName, {
    Object? arguments,
  }) {
    return navigator.currentState?.pushNamedAndRemoveUntil(
      routeName,
      (_) => false,
      arguments: arguments,
    );
  }

  Future<dynamic>? pushNamedAndRemoveUntil(
    String routeName,
    bool Function(Route<dynamic>) predicate, {
    Object? arguments,
  }) {
    return navigator.currentState?.pushNamedAndRemoveUntil(
      routeName,
      predicate,
      arguments: arguments,
    );
  }

  void goBack<T extends Object>([T? value]) {
    return navigator.currentState?.pop(value);
  }
}

